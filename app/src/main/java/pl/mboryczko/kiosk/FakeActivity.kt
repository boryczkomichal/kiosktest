package pl.mboryczko.kiosk

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class FakeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fake)
    }
}
